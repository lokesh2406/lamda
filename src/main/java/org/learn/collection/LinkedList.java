package org.learn.collection;

public class LinkedList<T> {

	private Node<T> head;
	private Node<T> tail;

	public void add(T element) {
		Node<T> nd = new Node<>();
		nd.setValue(element);
		if (head == null) {
			head = nd;
			tail = nd;
		} else {
			tail.setNextRef(nd);
			tail = nd;
		}
	}

	public void deleteAfter(T element) {

		Node<T> temp = head;
		Node<T> refNode = null;
		while (true) {
			if (temp == null) {
				break;
			}
			if (temp.compareTo(element) == 0) {
				refNode = temp;
				break;
			}
			temp = temp.getNextRef();
		}
		if (refNode != null) {
			temp = refNode.getNextRef();
			refNode.setNextRef(temp.getNextRef());
			if (refNode.getNextRef() == null) {
				tail = refNode;
			}
			System.out.println("Deleted: " + temp.getValue());
		} else {
			System.out.println("No such element found");
		}
	}
	
	public void delete(T element) {
		if(head==null) {
			System.out.println("List is empty");
		}
		if(head.getValue()==element) {
			head =head.getNextRef();
		}
		/*if(head==null) {

			return;
		}*/
		Node<T> current = head;
		while(current.getNextRef()!=null) {
			if(current.getNextRef().getValue()==element) {
				//current = current.getNextRef().getNextRef();
				current.setNextRef(current.getNextRef().getNextRef());
			}else {
				current = current.getNextRef();
			}
		}
	}

	public void addAfter(T element, T after) {
		Node<T> temp = head;
		Node<T> refNode = null;
		while (true) {
			if (temp == null) {
				break;
			}
			if (temp.compareTo(after) == 0) {
				refNode = temp;
				break;
			}
			temp = temp.getNextRef();
		}
		if (refNode != null) {
			Node<T> nd = new Node<>();
			nd.setValue(element);
			nd.setNextRef(temp.getNextRef());
			if (temp == tail) {
				tail = nd;
			}
			temp.setNextRef(nd);
		} else {
			System.out.println("No such element found");
		}
	}

	public void traverse() {

		Node<T> temp = head;
		while (true) {
			if (temp == null) {
				break;
			} else {
				System.out.println(temp.getValue());
				temp = temp.getNextRef();
			}
		}

	}

}
