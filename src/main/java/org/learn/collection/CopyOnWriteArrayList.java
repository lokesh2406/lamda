package org.learn.collection;

import java.util.ArrayList;
import java.util.List;

public class CopyOnWriteArrayList {

	//hskdvhjkshdkjvs
	//njkfjksdhfsdjkh
	public static void main(String[] args) {
		List<Integer> list = new ArrayList<Integer>();
		list.add(1);
		list.add(2);
		list.add(3);
		list.add(4);
		for(Integer i : list) {
			System.out.println(i);
			list.add(i);
		}
		System.out.println("===================");
		System.out.println(list);
	}
}
