package org.learn.collection;

public class ArrayListTest {

	public static void main(String[] args) {
		ArrayList<Integer> al = new ArrayList<>();
		al.add(1);
		al.add(2);
		al.add(4);
		al.add(5);
		al.add(6);
		al.add(6);
		al.add(7);
		al.add(10);
		al.add(11);
		al.add(12);
		al.add(13);
		al.add(14);
		al.add(15);
		al.add(31);
		al.add(113);
		al.add(321);
		al.add(3211);
		al.add(903);
		System.out.println(al);
		Integer integer = al.get(0);
		System.out.println(integer);
	}
}
