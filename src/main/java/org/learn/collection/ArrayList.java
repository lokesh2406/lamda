/**
 * 
 */
package org.learn.collection;

/**
 * @author sharma.l
 *
 */
public class ArrayList<T> {

	private int size;
	private int index = -1;
	private Object objects[];
	private float growth = 0.5f;

	public ArrayList() {
		size = 10;
		objects = new Object[size];
		java.util.ArrayList<Integer>  al;
	}

	public ArrayList(int size) {
		this.size = size;
		objects = new Object[size];
	}
	
	public void add(T obj) {
		
		if(index+1==size) {
			ensureCapacity();
		}
		objects[++index] = obj;
	}

	private void ensureCapacity() {

		int tempSize = (int) (size + (new Float(growth)*size));
		Object tempObj[] =  new Object[tempSize];
		size = tempSize;
		for(int i=0;i<objects.length;i++)
		{
			tempObj[i] = objects[i];
		}
		objects = tempObj;
		
	}
	
	@Override
	public String toString() {
		String str = "";
		for(int i=0;i<=index;i++) {
			str = str + "{"+objects[i].toString()+"}";
		}
		return str.toString();
	}
	
	
	public T get(int i) {
		if(i<0 || i>index)
			throw new ArrayIndexOutOfBoundsException("No value at positon");
		return (T) objects[i];
	}
	
	public int size() {
		return index+1;
	}
	
	public void remove() {
		index--;
	}
}
