package org.learn.collection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Hello {

	public static void main(String[] args) {
		Map<Employee,String> map = new HashMap<>();
		/*Employee e1 = new Employee("a");
		Employee e2 = new Employee("a");
		map.put(e1, "emp1");
		map.put(e2, "emp2");
		map.put(e1, "emp1 overrides");*/
		
		/*Employee e1 = new Employee("a");
		Employee e2 = new Employee("a");
		Employee e3 = new Employee("a");
		map.put(e1, "emp1");
		map.put(e2, "emp2");
		map.put(e3, "emp1 overrides");*/
		
		
		map.put(new Employee("a"), "Lokesh");
		map.put(new Employee("b"), "Ankur");
		map.put(new Employee("a"), "Lokesh Sharma");
		
		
		System.out.println("data"+map);
		System.out.println("size"+map.size());
		System.out.println(map.get(new Employee("a")));
		
		
		
	}
}
