package org.learn.collection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

public class EnumerationTest {

	public static void main(String[] args) {
		List<Integer> list = new Vector<>();
		list.add(1);
		list.add(2);
		list.add(3);
		list.add(4);
		list.add(2, 5);
		list.remove(100);
		
		
		Iterator<Integer> iterator = list.iterator();
		while(iterator.hasNext()) {
			int a=iterator.next();
			System.out.println(a);
		//	iterator.remove();
		}
	
		
		System.out.println("--------------");
		System.out.println(list);
		
	}
}
