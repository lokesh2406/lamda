package org.learn.collection;

public class Tiger {

	int color;
	int weight;
	public int getColor() {
		return color;
	}
	public void setColor(int color) {
		this.color = color;
	}
	public int getWeight() {
		return weight;
	}
	public void setWeight(int weight) {
		this.weight = weight;
	}
	@Override
	public String toString() {
		return "Tiger [color=" + color + ", weight=" + weight + "]";
	}
	/*@Override
	public int hashCode() {
		int result = 1;
		
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		
		return true;
	}*/
	public Tiger(int color, int weight) {
		super();
		this.color = color;
		this.weight = weight;
	}
	
	
}
