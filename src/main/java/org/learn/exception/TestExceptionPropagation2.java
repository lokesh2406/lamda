package org.learn.exception;

import java.io.IOException;

public class TestExceptionPropagation2 {

	void m(){  
	    try {
	    	int i =10/0;
			//throw new java.io.IOException("device error");
	    	throw new NullPointerException();
		} catch (NullPointerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}//checked exception  
	  }  
	  void n(){  
	    m();  
	  }  
	  void p(){  
	   try{  
	    n();  
	   }catch(Exception e){System.out.println("exception handeled");}  
	  }  
	  public static void main(String args[]){  
	   TestExceptionPropagation2 obj=new TestExceptionPropagation2();  
	   obj.p();  
	   System.out.println("normal flow");  
	  }  
}
