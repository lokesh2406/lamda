package org.learn.overlap;

import java.util.Date;

public class BookingDateRange {

	 private Date fromDate;
     private Date toDate;
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	@Override
	public String toString() {
		return "BookingDateRange [fromDate=" + fromDate + ", toDate=" + toDate + "]";
	}
	public BookingDateRange(Date fromDate, Date toDate) {
		super();
		this.fromDate = fromDate;
		this.toDate = toDate;
	}
	public BookingDateRange() {
		super();
	}
	
     
     
}
