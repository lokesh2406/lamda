package org.learn.overlap;

import static java.util.Comparator.comparing;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.learn.inheritance.Parent;

public class Test extends Parent {

	public static void main(String[] args) {

		Parent p = new Parent() {};
		Test t = new Test();
		List<BookingDateRange> dateRangeList = new ArrayList<>();
		dateRangeList.add(new BookingDateRange(new Date(2017, 0, 1), new Date(2017, 0, 26)));
		dateRangeList.add(new BookingDateRange(new Date(2017, 0, 12), new Date(2017, 0, 27)));
		dateRangeList.add(new BookingDateRange(new Date(2017, 0, 6), new Date(2017, 0, 31)));
		dateRangeList.add(new BookingDateRange(new Date(2017, 0, 17), new Date(2017, 1, 2)));

		boolean overlappingDates = isOverlappingDates(dateRangeList);
		System.out.println(overlappingDates);
		Optional<Date> maxDate = maxDate(dateRangeList);
		System.out.println(maxDate.get());
		Optional<Date> minDate = minDate(dateRangeList);
		System.out.println(minDate.get());
		List<BookingDateRange> sortedDate = sortedDate(dateRangeList);
	//	print(sortedDate);

	}
	private static List<BookingDateRange> sortedDate(List<BookingDateRange> dateRangeList) {

		List<BookingDateRange> collect = dateRangeList.stream().sorted(comparing(BookingDateRange::getFromDate))
				.collect(Collectors.toList());
		return collect;
	}

	private static Comparator<Date> d1 = Comparator.naturalOrder();

	private static void print(List<BookingDateRange> dateRangeList) {
		for (BookingDateRange b : dateRangeList) {

			System.out.println("From Date "+b.getFromDate()+" To Date "+b.getToDate());
		}

	}

	public static Optional<Date> maxDate(List<BookingDateRange> dateRangeList) {

		return dateRangeList.stream().map(o -> o.getFromDate()).collect(Collectors.maxBy(d1));

	}
	
	public static Optional<Date> minDate(List<BookingDateRange> dateRangeList) {

		return dateRangeList.stream().map(o -> o.getToDate()).collect(Collectors.minBy(d1));

	}

	public static boolean isOverlappingDates(List<BookingDateRange> dateRangeList) {

		boolean isOverlap = false;

		for (int index1 = 0; index1 < dateRangeList.size(); index1++) {
			for (int index2 = index1 + 1; index2 < dateRangeList.size(); index2++) {

				// Overlap exists if (StartA <= EndB) and (EndA >= StartB)

				Date startA = dateRangeList.get(index1).getFromDate();
				Date endA = dateRangeList.get(index1).getToDate();
				Date startB = dateRangeList.get(index2).getFromDate();
				Date endB = dateRangeList.get(index2).getToDate();

				boolean isStartABeforeEndB = (startA.compareTo(endB)) < 0;
				boolean isEndAAfterStartB = (endA.compareTo(startB)) > 0;

				boolean isCurrentPairOverlap = false;

				isCurrentPairOverlap = isStartABeforeEndB && isEndAAfterStartB;

				if (isCurrentPairOverlap) {
					isOverlap = true;
				} else {
					isOverlap = false;
					break;
				}

			}

			if (!isOverlap) {
				break;
			}
		}
		return isOverlap;

	}

}
