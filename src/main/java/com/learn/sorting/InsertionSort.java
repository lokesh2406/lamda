package com.learn.sorting;

/**
 * sorted- all items to the left are smaller
 * 
 * i= 1 to 5
 * 
 * 5 8 1 3 9 6
 * 
 * K=list[i]
 * 
 * j=i-1 to 0
 *
 */
public class InsertionSort {

	public static void main(String[] args) {

		int arr[] = { 5, 8, 1, 6, 9, 2 };
		arr = insertionSort(arr);
		for (int num : arr) {
			System.out.print(num + " , ");
		}
	}

	public static int[] insertionSort(int arr[]) {
		int i, j, temp, k;
		for (i = 1; i < arr.length; i++) {
			j = i - 1;
			k = arr[i];
			while (j >= 0 && k < arr[j]) {
				temp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = temp;
				j--;
			}
		}
		return arr;
	}

}
