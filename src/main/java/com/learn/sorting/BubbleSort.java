package com.learn.sorting;

/**
 * 
 * Bubble sort compares adjacent items in the list if the item on the left is
 * larger it swaps it to the right .
 *
 * Bubble sorting is not an efficient sorting algorithm because it uses nested
 * loops It is useful for only small data sets
 *
 * Its runs in O(n^2)
 */
public class BubbleSort {

	public static void main(String[] args) {

		int arr[] = { 5, 8, 1, 6, 9, 2 };
		arr = bubbleSort(arr);
		for (int num : arr) {
			System.out.print(num + " , ");
		}
	}

	public static int[] bubbleSort(int arr[]) {
		int i, j, temp;
		for (i = 0; i < arr.length - 1; i++) {
			for (j = 0; j < arr.length - 1 - i; j++) {
				if (arr[j] > arr[j + 1]) {
					temp = arr[j];
					arr[j] = arr[j + 1];
					arr[j + 1] = temp;
				}
			}
		}
		return arr;
	}

}
