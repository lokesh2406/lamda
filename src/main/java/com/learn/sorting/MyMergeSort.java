package com.learn.sorting;

public class MyMergeSort {

	public static void main(String[] args) {

		int[] inputArr = {45,23,11,89,77,98,4,28,65,43};
		int[] merge_sort = merge_sort(inputArr,0,inputArr.length-1);
		for(int q:merge_sort) {
			System.out.print(q+",");
		}
	}

	private static int[] merge_sort(int arr[], int start, int end) {
		if (arr.length == 1)
			return arr;
		int m = (start + end) / 2;
		int a[] = merge_sort(arr, start, m);
		System.out.println("Lokesh");
		int b[] = merge_sort(arr, m + 1, end);
		int c[] = merge(a, b);
		return c;
	}

	private static int[] merge(int[] a, int[] b) {

		int i = 0, j = 0, k = 0;
		int r[] = new int[a.length + b.length];
		while (i != a.length || j != b.length) {
			if (a[i] < b[j]) {
				r[k] = a[i];
				i++;
				k++;
			} else if (a[i] == b[j]) {
				r[k] = a[i];
				i++;
				k++;
				j++;
			} else if (a[i] > b[j]) {
				r[k] = b[j];
				j++;
				k++;
			}
		}
		if (i < a.length) {
			while (i != a.length) {
				r[k] = a[i];
				k++;
				i++;
			}
		} else if (j < b.length) {
			while (j != b.length) {
				r[k] = b[j];
				k++;
				j++;
			}
		}
		return r;
	}
}
