package com.learn.sorting;

public class SelectionSort {

	public static void main(String[] args) {
		int arr[] = { 7,8,5,4,9,2};
		arr = selection(arr);
		for (int num : arr) {
			System.out.print(num + " , ");
		}
	}

	public static int[] selection(int arr[]) {
		int minValue, minIndex, temp, i, j;
		for (i = 0; i < arr.length; i++) {
			minValue = arr[i];
			minIndex = i;
			for (j = i; j < arr.length; j++) {
				if (arr[j] < minValue) {
					minValue = arr[j];
					minIndex = j;
				}
			}
			if (arr[i] > minValue) {
				temp = arr[i];
				arr[i] = arr[minIndex];
				arr[minIndex] = temp;
			}
		}
		return arr;
	}
}
