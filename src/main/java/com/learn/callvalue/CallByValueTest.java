package com.learn.callvalue;

import java.util.ArrayList;
import java.util.List;

public class CallByValueTest {

	public static void main(String[] args) {
		List<Integer> list =new ArrayList<>();
		list.add(1);
		list.add(2);
		list.add(3);
		list.add(4);
		list.add(5);
		System.out.println("Size of array is "+list.size());
		ShareObject obj =new ShareObject(list);
		System.out.println("Size of array is "+list.size());
		
	}
}
