package com.learn.array;
/**
 * Binary search program  algorithm is in data structure book
 * @author sharma.l
 *
 */
public class BinarySearch {

	public static void main(String[] args) {

		int arr[]= {1,2,3,4,5,6,7,8,9};
		int binarySearch = binarySearch(arr,16);
		System.out.println(binarySearch);
	}

	private static int binarySearch(int arr[], int item) {

		int beg = 0;
		int end = arr.length - 1;
		int mid = (beg + end) / 2;
		int loc;
		while (beg <= end && arr[mid] != item) {

			if (item < arr[mid])
				end = mid - 1;
			else
				beg = mid + 1;

			mid = (beg + end) / 2;

		}
		if (arr[mid] == item)
			loc = mid;
		else
			loc = -1;

		return loc;
	}
}
