package com.learn.array;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * 
 * @author sharma.l
 *
 */
public class Logical {

	public static void main(String[] args) {

		int arr[] = { 1, 2, 3, 4, 5, 12 };
		Integer a[] = new Integer[] { 1, 2, 3, 4, 5, 6 };
		findThirdHighest(arr);
		duplicateNumber(arr);
		// How to remove duplicate number on Integer array in Java
		int[] removeDuplicate = removeDuplicate(new int[] { 3, 1, 3, 3, 1, 5, 6, 1, 2, 3, 4, 4, });
		for (int aw : removeDuplicate) {
			System.out.println("Remove deu" + aw);
		}
		// How to check if array contains a number in Java? (solution)
		boolean numberContain = isNumberContain(arr, 112);
		System.out.println(numberContain);
		// How to check if array contains a number in Java using ArrayList and generic
		boolean numberUsingArrays = isNumberUsingArrays(a, 6);
		System.out.println(numberUsingArrays);
		findLargestAndSmallest(new int[] { 32, 3, 243, 4, 3, 453, 4534, 54 });
		findThirdLargest(new int[] { 32, 3, 243, 4, 3, 453, 4534, 54 });
		/* nLargest(new int[] { 32, 3, 243, 4, 3, 453, 4534, 54 },1); */
		// nLargest(new int[] { 32, 3, 243, 4, 3, 453, 4534, 54 },1);
		nLargest(new Integer[] { 32, 3, 243, 4, 3, 453, 4534, 54 }, 1);
		nLargest(new String[] { "Apple", "Banana", "Carrot", "Donald" }, 1);
		// How to find all pairs on integer array whose sum is equal to given number?
		pairOfSum(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 }, 9);
		//
		Integer a1[] = { 1, 2, 3, 3, 4, 5 };
		Integer a2[] = { 1, 1, 12, 12, 15, 15, 15,15,18 };
		Integer a3[] = { 1, 212 };
		commenElementInThrreArrays(a1, a2, a3);
		// How to find first repeating element in array of integers
		firstRepeatingElement(a1);
		// How to find first non-repeating element in array of integers
		firstNonRepeatingElement(a2);

	}

	private static void firstNonRepeatingElement(Integer[] arr) {

		Arrays.sort(arr);
		int aa=arr.length;
		
		for (int i = 1; i < arr.length; i++) {
			
			if(  i!=aa && arr[i-1]!=arr[i] && arr[i+1]!=arr[i] ) {
				System.out.println("firstNonRepeatingElement"+arr[i]);
				break;
			}
		}

	}

	private static void firstRepeatingElement(Integer[] a1) {

		Arrays.sort(a1);
		for (int i = 1; i < a1.length; i++) {
			if (a1[i - 1] == a1[i]) {
				System.out.println(a1[i]);
			}
		}
	}

	private static void commenElementInThrreArrays(Integer arr[], Integer arr2[], Integer[] arr3) {
		List<Integer> one = new ArrayList<>(Arrays.asList(arr));
		List<Integer> two = new ArrayList<>(Arrays.asList(arr2));
		List<Integer> three = new ArrayList<>(Arrays.asList(arr3));

		one.retainAll(two);
		one.retainAll(three);
		System.out.println("common in all " + one);

	}

	private static void pairOfSum(int[] arr, int num) {

		Set<Integer> sp = new HashSet<>(arr.length);
		System.out.println(sp);

		for (int i = 0; i < arr.length; i++) {
			for (int j = i + 1; j < arr.length; j++) {
				for (int k = i + 2; k < arr.length; k++) {
					if (arr[i] + arr[j] + arr[k] == num) {
						System.out.println("{" + arr[i] + "," + arr[j] + "," + arr[k] + "}");
					}
				}
			}
		}

		pairOfSumSet(arr, num);

	}

	private static void pairOfSumSet(int[] arr, int num) {

		Set<Integer> set = new HashSet<>();
		for (int value : arr) {
			int target = num - value;
			if (!set.contains(target)) {
				set.add(value);
			} else {
				System.out.printf("(%d, %d) %n", value, target);
			}
		}
	}

	private static <T, V> void nLargest(T arr[], V n) {

		/*
		 * Set<Integer> collect = IntStream.of(arr).boxed().collect(Collectors.toSet());
		 * List<Integer> list = new ArrayList<>(collect);
		 * Collections.sort(list,Collections.reverseOrder()); System.out.println(list);
		 * Integer integer = list.get(n-1);
		 * System.out.println("Get the nth highest "+integer);
		 */

		List<T> collect = Arrays.asList(arr);
		Collections.sort(collect, Collections.reverseOrder());
		System.out.println(collect);
		T nHeighest = collect.get((int) n - 1);
		System.out.println("Get the nth highest " + nHeighest);

	}

	private static void findThirdLargest(int arr[]) {

		int largest = Integer.MIN_VALUE;
		int secondLargest = Integer.MIN_VALUE;
		int thirdLargest = Integer.MIN_VALUE;
		for (int num : arr) {
			if (num > largest) {
				thirdLargest = secondLargest;
				secondLargest = largest;
				largest = num;
			} else if (num > secondLargest) {
				thirdLargest = secondLargest;
				secondLargest = num;
			} else if (num > thirdLargest) {
				thirdLargest = num;
			}
		}
		System.out.println("First largest" + largest);
		System.out.println("Second largest" + secondLargest);
		System.out.println("Third largest" + thirdLargest);

	}

	private static void findLargestAndSmallest(int arrr[]) {

		int largest = Integer.MIN_VALUE;
		int smallest = Integer.MAX_VALUE;
		for (int number : arrr) {
			if (number > largest) {
				largest = number;
			} else if (number < smallest) {
				smallest = number;
			}
		}

		System.out.println("largest is " + largest);
		System.out.println("Smallest is " + smallest);

	}

	private static <T> boolean isNumberUsingArrays(T arr[], T obj) {
		return Arrays.asList(arr).contains(obj);
	}

	private static boolean isNumberContain(int arr[], int num) {

		int flag = 0;
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] == num) {
				flag = 1;
				break;
			}
		}
		if (flag == 0)
			return false;
		else
			return true;

	}

	private static int[] removeDuplicate(int arr[]) {

		int resultArr[] = new int[arr.length];
		Arrays.sort(arr);
		int index = 0;
		int previous = arr[0];
		for (int i = 1; i < arr.length; i++) {
			int check = arr[i];
			if (previous != check) {
				resultArr[index] = previous;
				index++;
			}
			previous = check;
		}
		return resultArr;

	}

	private static void findThirdHighest(int[] arr) {

		Arrays.sort(arr);
		int i = arr[0];
		System.out.println(i);

	}

	private static void duplicateNumber(int arr[]) {

		for (int i = 0; i < arr.length; i++) {
			for (int j = i + 1; j < arr.length; i++) {
				if (arr[i] == arr[j]) {
					System.out.println(arr[i]);
					break;
				}
			}
		}

	}
	
	/**
	 * Remove an element in an array
	 */
}
