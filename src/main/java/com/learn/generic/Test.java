package com.learn.generic;

import java.util.ArrayList;
import java.util.List;

public class Test {

	public static <T extends Comparable<T>> int compare(T t1, T t2){
		return t1.compareTo(t2);
	}
	
	public static <T> void printData(List<T> list){
		for(Object obj : list){
			System.out.print(obj + "::");
		}
	}
	public static double sum(List<? extends Number> list){
		double sum = 0;
		for(Number n : list){
			sum += n.doubleValue();
		}
		return sum;
	}
	
	public static double sum1(List<? extends Number> list){
		double sum = 0;
		for(Number n : list){
			sum += n.doubleValue();
		}
		System.out.println(sum);
		return sum;
	}
	public static double sum2(List<? super Integer> list){
		double sum = 0;
		
		return sum;
	}
	
	public static void main(String[] args) {
		int compare = compare("s","s");
		List<Integer> al = new ArrayList<>();
		al.add(1);
		al.add(1);
		al.add(1);
		al.add(1);
		sum1(al);
	}
}
