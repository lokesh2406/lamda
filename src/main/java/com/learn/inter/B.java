package com.learn.inter;

public interface B extends A {

	public void lokesh();
	default void hello() {
		System.out.println("Default hello() method from B interface");
	}
}
