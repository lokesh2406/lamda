package com.learn.blockingqueue;

import java.util.concurrent.BlockingQueue;

public class Producer implements Runnable {

	BlockingQueue<Integer> queue;

	public Producer(BlockingQueue<Integer> queue) {
		this.queue = queue;
	}

	@Override
	public void run() {
		for (int i = 1; i <= 40; i++) {

			produce(i);
		}
	}

	private void produce(Integer str) {

		try {
			if (queue.size() ==3) {
				System.out.println(
						"Queue is full " + Thread.currentThread().getName() + " is waiting for cosumer to consume");
			}
			System.out.println("Produce : " + str);
			queue.put(str);
			
			
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			System.out.println(e);
		}

	}

}
