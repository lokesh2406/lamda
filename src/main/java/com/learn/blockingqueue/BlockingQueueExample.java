package com.learn.blockingqueue;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class BlockingQueueExample {

	public static void main(String[] args) throws InterruptedException {

		BlockingQueue<Integer> queue = new ArrayBlockingQueue<>(2);
		queue.put(100);
		queue.put(200);
		queue.put(300);
		Producer producer = new Producer(queue);
	    Consumer consumer = new Consumer(queue);
	    new Thread(producer,"Producer").start();
	    new Thread(consumer,"consumer").start();
	}
}

