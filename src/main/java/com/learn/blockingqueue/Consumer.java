package com.learn.blockingqueue;

import java.util.concurrent.BlockingQueue;

public class Consumer implements Runnable {

	BlockingQueue<Integer> queue;

	public Consumer(BlockingQueue<Integer> queue) {
		this.queue = queue;
	}

	public void run() {
		while (true) {
			consume();
		}
	}

	private void consume() {

		try {
			Thread.sleep(3000);
			if (queue.size() ==3) {
				System.out.println(
						"Queue is empty " + Thread.currentThread().getName() + " is waiting for producer to produce");
			}
			System.out.println("consumed by  " + Thread.currentThread().getName() + " " + queue.take());
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			System.out.println(e);
		}

	}
}
