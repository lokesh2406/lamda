package com.learn.constructor;

public class Parent {

	protected Parent() {
		System.out.println("Parent class constructor is called");
	}
	public void parentDisp() {
		System.out.println("ParentDisp called");
	}
}
