package com.learn.lambda;

import static java.util.Comparator.comparing;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.LongStream;
import java.util.stream.Stream;

import com.learn.lambda.Dish.Type;

public class Test {

	Trader raoul = new Trader("Raoul", "Cambridge");
	Trader mario = new Trader("Mario", "Milan");
	Trader alan = new Trader("Alan", "Cambridge");
	Trader brian = new Trader("Brian", "Cambridge");

	List<Transaction> transactions = Arrays.asList(new Transaction(brian, 2011, 300),
			new Transaction(raoul, 2012, 1000), new Transaction(raoul, 2011, 400), new Transaction(mario, 2012, 710),
			new Transaction(mario, 2012, 700), new Transaction(alan, 2012, 950));

	List<Dish> menu = Arrays.asList(new Dish("pork", false, 800, Dish.Type.MEAT),
			new Dish("beef", false, 700, Dish.Type.MEAT), new Dish("chicken", false, 400, Dish.Type.MEAT),
			new Dish("french fries", true, 530, Dish.Type.OTHER), new Dish("rice", true, 350, Dish.Type.OTHER),
			new Dish("season fruit", true, 120, Dish.Type.OTHER), new Dish("pizza", true, 550, Dish.Type.OTHER),
			new Dish("prawns", false, 300, Dish.Type.FISH), new Dish("salmon", false, 450, Dish.Type.FISH));

	static ArrayList<Integer> al =new ArrayList<>();
	private static Predicate<Transaction> predicate = obj -> obj.getYear() == 2011;
	
	/*public Predicate<ScheduleGroupType> getScheduleGroupTypePredicate(String scheduleGroupTypeValue){
        Predicate<ScheduleGroupType> junctionPredicate = scheduleGroupType -> {
             String scheduleGroupTypeName = scheduleGroupType.getScheduleGroupTypeName().toUpperCase();
             if (scheduleGroupTypeName.equalsIgnoreCase(scheduleGroupTypeValue)) {
                  return true;
             } else {
                  return false;
             }
        };
        return junctionPredicate;
  }*/

	
	/*public Predicate<Transaction> getScheduleGroupTypePredicate(ArrayList<Integer> list){
        Predicate<Transaction> junctionPredicate = obj -> {
        	int year = obj.getYear();
        	list.forEach(ob->{
        		if(ob==year)
        			return true;
        		else
        			return false;
        	});
        };
        return junctionPredicate;
  }

	*/

	private Comparator<Transaction> tranxComparator = Comparator.comparingInt(Transaction::getValue);
	private Comparator<Date> d1 = Comparator.naturalOrder();
	
	
	

	public static void main(String[] args) {

		Test test = new Test();
		// Find all transactions in the year 2011 and sort them by value (small to
		// high).
		test.allTransaction2011(predicate);
		// What are all the unique cities where the traders work?
		test.allUniqueCities();
		// Find all traders from Cambridge and sort them by name
		test.allTraderFrom();
		// Return a string of all traders� names sorted alphabetically
		test.allTraderString();
		// Are any traders based in Milan?
		test.isTraderMilan();
		// Print all transactions� values from the traders living in Cambridge
		test.allLiveIn();
		// What�s the highest value of all the transactions?
		test.higestValue();
		// Find the transaction with the smallest value
		test.getTransactionWithSmallesValue();
		// mapInt
		test.mapInt();
		//
		test.infiniteStream();
		// count
		test.count();
		// get Max value object from transaction
		test.getMaxValue();
		// 6.2.2. Summarization
		test.getTotalValueOfTransaction();
		// joining();
		test.joining();
		// Grouping
		test.grouping();
		// grouping without method reference
		test.groupingWithOutMethodReferecne();
		// Multi-level grouping
		test.multiLevelGroup();
		//
		test.groupBY();
		// partitoned
		test.partition();
		//
		// measureSumPerf(adder, 50L);
	}

	private void partition() {

		Map<Boolean, List<Dish>> collect = menu.stream().collect(Collectors.partitioningBy(Dish::isVegetarian));
		// List of vegetarian
		List<Dish> list = collect.get(true);
		System.out.println(list);
		// By using filter we can also find all vege kist
		List<Dish> vegeList = menu.stream().filter(Dish::isVegetarian).collect(Collectors.toList());
		int availableProcessors = Runtime.getRuntime().availableProcessors();
		System.out.println("Available processors are : " + availableProcessors);

	}

	private void multiLevelGroup() {

		Map<Type, Map<CaloricLevel, List<Dish>>> multiGroup = menu.stream()
				.collect(Collectors.groupingBy(Dish::getType, Collectors.groupingBy(dish -> {
					if (dish.getCalories() <= 400)
						return CaloricLevel.DIET;
					else if (dish.getCalories() <= 700)
						return CaloricLevel.NORMAL;
					else
						return CaloricLevel.FAT;
				})));
		System.out.println(multiGroup);
	}

	private void groupingWithOutMethodReferecne() {

		Map<CaloricLevel, List<Dish>> group = menu.stream().collect(Collectors.groupingBy(dish -> {
			if (dish.getCalories() <= 400)
				return CaloricLevel.DIET;
			else if (dish.getCalories() <= 700)
				return CaloricLevel.NORMAL;
			else
				return CaloricLevel.FAT;
		}));
		System.out.println(group);

	}

	public void groupBY() {
		Map<Type, Long> collect = menu.stream().collect(Collectors.groupingBy(Dish::getType, Collectors.counting()));
		System.out.println(collect);
	}

	public enum CaloricLevel {
		DIET, NORMAL, FAT
	}

	private void grouping() {
		/*
		 * Map<Integer, List<Transaction>> collect = transactions.stream()
		 * .collect(Collectors.groupingBy(Transaction::getYear));
		 */

		Map<Integer, List<Transaction>> collect = transactions.stream()
				.collect(Collectors.groupingBy(t -> t.getYear()));
		System.out.println(collect);

	}

	private void joining() {

		String year = transactions.stream().map(a -> a.getYear() + "").collect(Collectors.joining(","));
		System.out.println(year);

	}

	private void getTotalValueOfTransaction() {

		Integer reduce = transactions.stream().map(q -> q.getValue()).reduce(0, (a, b) -> a + b);
		System.out.println(reduce);

		IntSummaryStatistics collect = transactions.stream().collect(Collectors.summarizingInt(Transaction::getValue));
		System.out.println(collect.toString());
	}

	private void getMaxValue() {

		Optional<Transaction> collect = transactions.stream().collect(Collectors.maxBy(tranxComparator));
		System.out.println(collect.get());
	}

	private void count() {

		long count = transactions.stream().count();
	}

	private void infiniteStream() {

		// Stream.iterate(0,n->n+2).forEach(System.out::println);
		/*
		 * Stream.iterate(new int[]{0, 1},t->new int[]{t[1],t[0]+t[1]}).limit(20)
		 * .forEach(t->System.out.println(t[0]+" "+t[1]));
		 */

		Stream.iterate(new int[] { 0, 1 }, t -> new int[] { t[1], t[0] + t[1] }).limit(20).map(t -> t[0])
				.forEach(System.out::println);
	}

	private void mapInt() {

		// OptionalInt mapToInt =
		// transactions.stream().mapToInt(obj->obj.getValue()).max();
		int mapToInt = transactions.stream().mapToInt(obj -> obj.getValue()).sum();
		System.out.println(mapToInt);
	}

	private void getTransactionWithSmallesValue() {

		Optional<Transaction> reduce = transactions.stream().reduce((a, b) -> a.getValue() < b.getValue() ? a : b);
		System.out.println(reduce.get());
	}

	private void higestValue() {

		Optional<Integer> reduce = transactions.stream().map(o -> o.getValue()).reduce(Integer::max);
		System.out.println(reduce.get());

	}

	private void allLiveIn() {
		transactions.stream().filter(o -> o.getTrader().getCity().equals("Cambridge")).map(Transaction::getValue)
				.forEach(System.out::println);
	}

	private void isTraderMilan() {

		boolean anyMatch = transactions.stream().anyMatch(obj -> obj.getTrader().getName().equals("Raoul"));
		System.out.println(anyMatch);
	}

	private void allUniqueCities() {

		List<String> collect = transactions.stream().map(obj -> obj.getTrader().getCity()).distinct()
				.collect(Collectors.toList());
		System.out.println(collect);
	}

	private void allTransaction2011(Predicate<Transaction> predicate) {

		List<Transaction> collect = transactions.stream().filter(predicate).sorted(comparing(Transaction::getValue))
				.collect(Collectors.toList());
		System.out.println(collect);
	}

	private void allTraderFrom() {

		List<Trader> collect = transactions.stream().map(obj -> obj.getTrader())
				.filter(obj -> obj.getCity().equals("Cambridge")).distinct().sorted(comparing(Trader::getName))
				.collect(Collectors.toList());
		System.out.println(collect);
	}

	private void allTraderString() {

		String reduce = transactions.stream().map(o -> o.getTrader().getName()).distinct().sorted().reduce("",
				(q, r) -> q + " " + r);
		System.out.println(reduce);

	}

	public static long measureSumPerf(Function<Long, Long> adder, long n) {
		long fastest = Long.MAX_VALUE;
		for (int i = 0; i < 10; i++) {
			long start = System.nanoTime();
			long sum = adder.apply(n);
			long duration = (System.nanoTime() - start) / 1_000_000;
			System.out.println("Result: " + sum);
			if (duration < fastest)
				fastest = duration;
		}
		return fastest;
	}

	public static long parallelRangedSum(long n) {
		return LongStream.rangeClosed(1, n).parallel().reduce(0L, Long::sum);
	}

}
