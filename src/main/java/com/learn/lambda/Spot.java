package com.learn.lambda;

import java.util.Date;

public class Spot {

	private int id;
	private String audiance;
	private String txlevelId;
	private Date date;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAudiance() {
		return audiance;
	}
	public void setAudiance(String audiance) {
		this.audiance = audiance;
	}
	public String getTxlevelId() {
		return txlevelId;
	}
	public void setTxlevelId(String txlevelId) {
		this.txlevelId = txlevelId;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	@Override
	public String toString() {
		return "Spot [id=" + id + ", audiance=" + audiance + ", txlevelId=" + txlevelId + ", date=" + date + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((audiance == null) ? 0 : audiance.hashCode());
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + id;
		result = prime * result + ((txlevelId == null) ? 0 : txlevelId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Spot other = (Spot) obj;
		if (audiance == null) {
			if (other.audiance != null)
				return false;
		} else if (!audiance.equals(other.audiance))
			return false;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (id != other.id)
			return false;
		if (txlevelId == null) {
			if (other.txlevelId != null)
				return false;
		} else if (!txlevelId.equals(other.txlevelId))
			return false;
		return true;
	}
	public Spot(int id, String audiance, String txlevelId, Date date) {
		super();
		this.id = id;
		this.audiance = audiance;
		this.txlevelId = txlevelId;
		this.date = date;
	}
	
	
	
	
	
}
