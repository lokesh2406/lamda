package com.learn.thread;
/**
 * 
 * @author sharma.l
 *Direct implementation of Runnable
 */
public class MyRunnable implements Runnable {

	@Override
	public void run() {
		System.out.println("Run() of MyRunnable");
		Thread th = Thread.currentThread();
		th.setName("Lokesh");
		for(int i=0;i<5;i++) {
			System.out.println(th.getName()+" thread "+i);
			try {
				Thread.sleep(1000);
			}
			catch(Exception e) {}
		}
		
		System.out.println(th.getName()+" thread is completed");
	}

}
