package com.learn.thread;

import java.util.List;

public class Printer {

	private List<String> data;

	public synchronized void printData(List<String> data,String s) {
		this.data = data;
		data.forEach(obj -> {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println(s +" : "+ obj);
		});

	}
}
