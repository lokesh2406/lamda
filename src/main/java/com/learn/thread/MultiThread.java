package com.learn.thread;

public class MultiThread {

	public static void main(String[] args) {
		Thread th = Thread.currentThread();
		System.out.println("main() is executing in " + th.getName() + " thread");
		// create a MyRunnable object to repsent the worker
		MyRunnable r = new MyRunnable();
		MyRunnable1 r1 = new MyRunnable1();
		// invoke the run method of MyRunnable directly
		System.out.println("Control is back in main thread");
		// creating a Thread object to represent the manager
		Thread t = new Thread(r);
		t.start();
		Thread t1 = new Thread(r1);
		t1.start();
		for (int i = 0; i < 5; i++) {
			System.out.println(th.getName() + " thread " + i);
			try {
				Thread.sleep(1000);
			} catch (Exception e) {
			}
		}

	}
}
