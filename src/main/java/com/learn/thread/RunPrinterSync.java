package com.learn.thread;

public class RunPrinterSync {

	public static void main(String[] args) {
		Printer printer = new Printer();
		PrinterThread1 t1 =new PrinterThread1(printer, "one");
		PrinterThread2 t2 =new PrinterThread2(printer, "two");
		PrinterThread3 t3 =new PrinterThread3(printer, "three");
	}
}
