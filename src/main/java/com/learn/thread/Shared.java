package com.learn.thread;

public class Shared {

	int x,y;
	public synchronized void show(String s,int a) {
		x=a;
		System.out.println("Starting in method "+s+" "+x);
		try {
			Thread.sleep(2000);
		}catch(Exception e) {}
		System.out.println("Exits from method "+s+" "+x);
	}
	
	public synchronized int add(int a,int b) {
		x=a;
		y=b;
		try {
			Thread.sleep(2000);
		}catch(Exception e) {}
		return x+y;
	}
	
}
