package com.learn.thread.deadlock;

import java.util.List;

public class Consumer implements Runnable {

	private List<Integer> list;
	private Thread producer;
	
	public Consumer(List<Integer> list) {
		this.list=list;
	}

	@Override
	public void run() {
		try {
			consume();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void consume() throws InterruptedException {

		synchronized (list) {

			while (true) {
				if (list.size() == 0) {
					System.out.println("Queue is empty " + Thread.currentThread().getName()
							+ " is waiting for producer to produce");
					Thread.currentThread().suspend();
				}
				Thread.sleep(1000);
				Integer i = list.remove(0);
				System.out.println("consumed by  " + Thread.currentThread().getName() + " " + i);

			}

		}

	}

}
