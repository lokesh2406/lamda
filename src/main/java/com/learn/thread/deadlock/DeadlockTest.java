package com.learn.thread.deadlock;

import java.util.ArrayList;
import java.util.List;

public class DeadlockTest {

	public static void main(String[] args) {
	
		List<Integer> list = new ArrayList<>();
		Producer produce = new Producer(list);
		Consumer consumer = new Consumer(list);
		Thread p = new Thread(produce, "Produce");
		Thread c =new Thread(consumer,"consue");
		p.setPriority(Thread.MAX_PRIORITY);
		p.start();
		c.start();
	}
}
