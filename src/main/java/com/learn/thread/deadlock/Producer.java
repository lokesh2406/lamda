package com.learn.thread.deadlock;

import java.util.List;

public class Producer implements Runnable {
	private static final int MAX = 2;
	private List<Integer> list;
	public Producer(List<Integer> list) {
		super();
		this.list = list;
	}

	@Override
	public void run() {
		for (int i = 1; i <= 10; i++) {
			try {
				produce(i);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	private void produce(int i) throws InterruptedException {
		synchronized (list) {
			while (i == MAX) {
				System.out.println("Queue is full " + Thread.currentThread().getName() + " is waiting ");
				Thread.currentThread().suspend();
			}
		}
		Thread.sleep(1000);
		System.out.println("Produce : " + i);
		list.add(i);
	}
}
