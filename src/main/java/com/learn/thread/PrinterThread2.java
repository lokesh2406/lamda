package com.learn.thread;

import java.util.Arrays;
import java.util.List;

public class PrinterThread2 extends Thread {

	Printer printer;

	public PrinterThread2(Printer printer, String str) {
		super(str);
		this.printer = printer;
		start();
	}

	@Override
	public void run() {
		List<String> list = Arrays.asList(new String[] { "PrinterThread2 task 1", "PrinterThread2 task 2", "PrinterThread2 task 3",
				"PrinterThread2 task 4", "PrinterThread2 task 5", });
		printer.printData(list,Thread.currentThread().getName());
	}
}
