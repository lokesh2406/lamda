package com.learn.thread;

public class MyRunnable1 implements Runnable {


	@Override
	public void run() {
		System.out.println("Run() of MyRunnable1");
		Thread th = Thread.currentThread();
		th.setName("Shikha");
		for(int i=0;i<5;i++) {
			System.out.println(th.getName()+" thread "+i);
			try {
				Thread.sleep(1000);
			}
			catch(Exception e) {}
		}
		
		System.out.println(th.getName()+" thread is completed");
	}


}
