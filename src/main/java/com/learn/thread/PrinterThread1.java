package com.learn.thread;

import java.util.Arrays;
import java.util.List;

public class PrinterThread1 extends Thread {

	Printer printer;

	public PrinterThread1(Printer printer, String str) {
		super(str);
		this.printer = printer;
		start();
	}

	@Override
	public void run() {
		List<String> list = Arrays.asList(new String[] { "PrinterThread1 task 1", "PrinterThread1 task 2", "PrinterThread1 task 3",
				"PrinterThread1 task 4", "PrinterThread1 task 5", });
		printer.printData(list,Thread.currentThread().getName());
	}
}
