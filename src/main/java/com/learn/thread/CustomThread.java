package com.learn.thread;

public class CustomThread extends Thread {

	Shared s;
	public CustomThread(Shared s,String str) {
		super(str);
		this.s=s;
		start();
	}
	@Override
	public void run() {
		s.show(Thread.currentThread().getName(),10);
		System.out.println("Thread1 sum of 10,20 = "+s.add(10, 20));
	}
}
