package com.learn.thread;

public class ThreadA {

	public static void main(String[] args) throws InterruptedException {
		
		
		ThreadB b = new ThreadB();
		b.start();
		ThreadC c = new ThreadC();
		c.start();
		
		synchronized (b) {
			System.out.println("Inside "+Thread.currentThread().getName());
			System.out.println("Wait for  b to complete");
			/*if(b.total==0) {
				System.out.println("Waiting on b");
				b.wait();
				
			}else {
				System.out.println("Inside else");
			}*/
			b.wait();
			
			System.out.println("Total is " + b.total);
		}
		System.out.println("Lokesh Sharma");
		

	}
}
