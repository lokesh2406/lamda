package com.learn.thread;

import java.util.Arrays;
import java.util.List;

public class PrinterThread3 extends Thread {

	Printer printer;

	public PrinterThread3(Printer printer, String str) {
		super(str);
		this.printer = printer;
		start();
	}

	@Override
	public void run() {
		List<String> list = Arrays.asList(new String[] { "PrinterThread3 task 1", "PrinterThread3 task 2", "PrinterThread3 task 3",
				"PrinterThread3 task 4", "PrinterThread3 task 5", });
		printer.printData(list,Thread.currentThread().getName());
	}
}
