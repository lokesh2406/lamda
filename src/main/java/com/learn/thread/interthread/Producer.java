package com.learn.thread.interthread;

import java.util.List;

public class Producer implements Runnable {

	private List<Integer> queueData;
	private int MAX_LIMIT = 4;

	public Producer(List<Integer> queueData) {
		this.queueData = queueData;
	}

	@Override
	public void run() {

		for (int i = 1; i <= 40; i++) {
			try {
				produce(i);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	private void produce(int i) throws InterruptedException {

		synchronized (queueData) {
			while (queueData.size() == MAX_LIMIT) {

				System.out.println("Queue is full " + Thread.currentThread().getName() + " is waiting ");
				queueData.wait();
			}
		//	Thread.sleep(1000);
			queueData.add(i);
			System.out.println("Produce : " + i);
			queueData.notify();
			/**
			 * No -- notify/notifyAll don't release locks like wait does. The awakened
			 * thread can't run until the code which called notify releases its lock.
			 */

		}
	}

}
