package com.learn.thread.interthread;

import java.util.List;

public class Consumer implements Runnable {

	private List<Integer> queueData;

	public Consumer(List<Integer> queueData) {
		this.queueData = queueData;
	}

	@Override
	public void run() {

		while (true) {
			try {
				consume();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}

	private void consume() throws InterruptedException {
		synchronized (queueData) {

			while (queueData.size() == 0) {
				System.out.println(
						"Queue is emplty " + Thread.currentThread().getName() + " is waiting for producer to produce");
				queueData.wait();
			}
		//	 Thread.sleep(1000);
			Integer i = queueData.remove(0);
			queueData.notifyAll();
			System.out.println("consumed by  "+Thread.currentThread().getName()+" " + i);
			
		}
	}

}
