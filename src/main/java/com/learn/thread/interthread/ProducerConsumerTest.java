package com.learn.thread.interthread;

import java.util.ArrayList;
import java.util.List;

public class ProducerConsumerTest {

	public static void main(String[] args) {
		List<Integer> queueData = new ArrayList<>();
		Producer produce = new Producer(queueData);
		Consumer consumer = new Consumer(queueData);
		Thread producerThread =new Thread(produce,"Producer");
		Thread consumerThread = new Thread(consumer,"Consumer");
		consumerThread.start();
		producerThread.start();
	}
}
